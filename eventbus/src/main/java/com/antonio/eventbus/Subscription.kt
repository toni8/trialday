package com.antonio.eventbus

data class Subscription(val subscriberClass: Any, val listener: Listener)