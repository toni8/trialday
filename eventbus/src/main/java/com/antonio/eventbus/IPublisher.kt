package com.antonio.eventbus

import kotlin.reflect.KClass
import kotlin.reflect.KFunction

interface IPublisher {
    fun publish(event: Event<*>)
}