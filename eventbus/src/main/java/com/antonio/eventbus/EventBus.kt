package com.antonio.eventbus

import kotlin.reflect.KClass

object EventBus : ISubscriber, IPublisher {

    private val lock = Any()
    private val subscriptions = mutableSetOf<Subscription>()

    override fun subscribe(
        subscriberClass: KClass<*>,
        priority: Priority,
        event: KClass<*>,
        action: (Event<*>) -> Unit
    ) {
        synchronized(lock) {
            val existingOrNewSubscription =
                subscriptions.firstOrNull { it.subscriberClass == subscriberClass }
                    ?.also { subscriber ->
                        subscriber.listener.actions[event] = action
                    }
                    ?: Subscription(
                        subscriberClass,
                        Listener(priority, mutableMapOf(event to action))
                    )

            subscriptions.add(existingOrNewSubscription)
            println("subs: ${subscriptions.size}")
        }
    }

    override fun unsubscribe(subscriberClass: KClass<*>) {
        synchronized(lock) {
            subscriptions.removeIf { it.subscriberClass == subscriberClass }
        }
    }

    override fun clean() {
        synchronized(lock) {
            subscriptions.clear()
        }
    }

    override fun publish(event: Event<*>) {
        synchronized(lock) {
            println("event class: ${event::class}")

            val subscriptions =
                subscriptions.filter { it.listener.actions.containsKey(event::class) }

            subscriptions.sortedByDescending { it.listener.actionPriority.ordinal }
                .forEach { subscription ->
                    subscription.listener.actions[event::class]?.invoke(event)
                }
        }
    }

    override fun subscriptionExists(subscriberClass: KClass<*>) = synchronized(lock) {
        !subscriptions.none { it.subscriberClass == subscriberClass }
    }

}