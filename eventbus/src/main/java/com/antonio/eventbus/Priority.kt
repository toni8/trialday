package com.antonio.eventbus

enum class Priority {
    LOW,
    NORMAL,
    HIGH
}