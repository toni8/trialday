package com.antonio.eventbus

import kotlin.reflect.KClass

interface ISubscriber {
    fun subscribe(subscriberClass: KClass<*>, priority: Priority, event: KClass<*>, action: (Event<*>) -> Unit)
    fun unsubscribe(subscriberClass: KClass<*>)
    fun clean()

    fun subscriptionExists(subscriberClass: KClass<*>): Boolean
}