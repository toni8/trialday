package com.antonio.eventbus

abstract class Event<T> constructor(private val data: Any?) {
    override fun toString(): String {
        return data.toString()
    }
}