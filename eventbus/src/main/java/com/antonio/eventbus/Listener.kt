package com.antonio.eventbus

import kotlin.reflect.KClass

class Listener(
    var actionPriority: Priority = Priority.NORMAL,
    val actions: MutableMap<KClass<*>, (Event<*>) -> Unit>
)