package com.antonio.eventbus

import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import kotlin.reflect.KClass

class EventBusTest {

    private val intData1 = 100
    private val intData2 = 200
    private val stringData1 = "some string"
    private val stringData2 = "another string"

    private val lock = Any()
    private val publishedEvents = arrayListOf<Event<*>>()

    inner class ListenerExampleLow(private val events: List<KClass<*>>) {
        fun subscribe() {
            synchronized(lock) {
                events.forEach { event ->
                    EventBus.subscribe(
                        subscriberClass = this@ListenerExampleLow::class,
                        priority = Priority.LOW,
                        event = event,
                        action = {
                            println("event published: $it , from ${this@ListenerExampleLow::class.simpleName}")
                            publishedEvents.add(it)
                        }
                    )
                }

            }
        }

        fun unsubscribe() {
            synchronized(lock) {
                EventBus.unsubscribe(this@ListenerExampleLow::class)
            }
        }
    }

    inner class ListenerExampleNormal(private val events: List<KClass<*>>) {
        fun subscribe() {
            synchronized(lock) {
                events.forEach { event ->
                    EventBus.subscribe(
                        subscriberClass = this@ListenerExampleNormal::class,
                        priority = Priority.NORMAL,
                        event = event,
                        action = {
                            println("event published: $it , from ${this@ListenerExampleNormal::class.simpleName}")
                            publishedEvents.add(it)
                        }
                    )
                }

            }
        }

        fun unsubscribe() {
            synchronized(lock) {
                EventBus.unsubscribe(this@ListenerExampleNormal::class)
            }
        }
    }

    private val lowPriorityListener =
        ListenerExampleLow(listOf(IntEvent::class, StringEvent::class))

    private val normalPriorityListener =
        ListenerExampleNormal(listOf(IntEvent::class, StringEvent::class))

    @Before
    fun init() {
        lowPriorityListener.subscribe()
        normalPriorityListener.subscribe()
    }

    @After
    fun clean() {
        EventBus.clean()
        publishedEvents.clear()
    }

    @Test
    fun testBasicSubscription() {
        Assert.assertTrue(EventBus.subscriptionExists(lowPriorityListener::class))
        Assert.assertTrue(EventBus.subscriptionExists(normalPriorityListener::class))
    }

    @Test
    fun testBasicUnsubscription() {
        Assert.assertTrue(EventBus.subscriptionExists(lowPriorityListener::class))
        lowPriorityListener.unsubscribe()
        Assert.assertFalse(EventBus.subscriptionExists(lowPriorityListener::class))
        Assert.assertTrue(EventBus.subscriptionExists(normalPriorityListener::class))
    }

    @Test
    fun testDisposition() {
        clean()
        Assert.assertFalse(EventBus.subscriptionExists(lowPriorityListener::class))
        Assert.assertFalse(EventBus.subscriptionExists(normalPriorityListener::class))
    }

    @Test
    fun testEventPayload() {

        val intEvent = IntEvent(intData1)
        val stringEvent = StringEvent(stringData1)

        Assert.assertEquals(intEvent.mData, intData1)
        Assert.assertEquals(stringEvent.mData, stringData1)

        EventBus.publish(intEvent)
        EventBus.publish(stringEvent)

        Assert.assertTrue(publishedEvents.contains(intEvent))
        Assert.assertTrue(publishedEvents.contains(stringEvent))
    }


    @Test
    fun testBasicEventPublish() {
        val intEvent1 = IntEvent(intData1)
        val intEvent2 = IntEvent(intData2)

        EventBus.publish(intEvent1)

        Assert.assertTrue(publishedEvents.contains(intEvent1))
        Assert.assertFalse(publishedEvents.contains(intEvent2))
    }

}

data class IntEvent(val mData: Int?) : Event<Int>(mData)
data class StringEvent(val mData: String?) : Event<String>(mData)